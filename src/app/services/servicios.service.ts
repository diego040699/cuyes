import { Injectable } from '@angular/core';
import { environment } from "../../environments/environment";
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ServiciosService {

  private api = environment.api
  constructor(private http: HttpClient) { }

  registerUser(data) {
    const url = `${this.api}registerUser`;
    return this.http.post(url, data);
  }
  listProductos(data) {
    const url = `${this.api}productos`;
    return this.http.post(url, data);
  }
  detalleProducto(data) {
    const url = `${this.api}detalleProducto`;
    return this.http.post(url, data);
  }

  medios() {
    const url = `${this.api}medios`;
    return this.http.get(url);
  }

  pedidoRegister(data) {
    const url = `${this.api}pedidoRegister`;
    return this.http.post(url, data);
  }

  updateperfil(data, id) {
    const url = `${this.api}user/${id}`;
    return this.http.put(url, data);
  }

  listStock() {
    const url = `${this.api}stock`;
    return this.http.get(url);
  }

  updateStock(data, id) {
    const url = `${this.api}stock/${id}`;
    return this.http.put(url, data);
  }

  updateStockAll(data) {
    const url = `${this.api}stockAll`;
    return this.http.post(url, data);
  }

  getUser(id) {
    const url = `${this.api}user/${id}`;
    return this.http.get(url);
  }

  getInformacion() {
    const url = `${this.api}cuyinformacion`;
    return this.http.get(url);
  }

  getEstados() {
    const url = `${this.api}estados`;
    return this.http.get(url);
  }

  getHistorial(data) {
    const url = `${this.api}cuyhistoria`;
    return this.http.post(url, data);
  }

  filtro_historial_fecha(data) {
    const url = `${this.api}filtro_historial_fecha`;
    return this.http.post(url, data);
  }

  filtro_historial_estado(data) {
    const url = `${this.api}filtro_historial_estado`;
    return this.http.post(url, data);
  }

  getPedido(id) {
    const url = `${this.api}pedido/${id}`;
    return this.http.get(url);
  }
  getDistritos() {
    const url = `${this.api}distritos`;
    return this.http.get(url);
  }

  getGrupoCliente() {
    const url = `${this.api}listar_grupocliente`;
    return this.http.get(url);
  }

  cancelarPedido(id) {
    const url = `${this.api}cancelarPedido/${id}`;
    return this.http.get(url);
  }

  resumenPedidoCliente() {
    const url = `${this.api}resumen_pedido_cliente`;
    return this.http.get(url);
  }
  filtroResumenPedidoCliente(data) {
    const url = `${this.api}filtro_resumen_pedido_cliente`;
    return this.http.post(url, data);
  }
  resumenPedidoCategoria() {
    const url = `${this.api}resumen_pedido_categoria`;
    return this.http.get(url);
  }
  filtroResumenPedidoCategoria(data) {
    const url = `${this.api}filtro_resumen_pedido_categoria`;
    return this.http.post(url, data);
  }
  resumenPedidoDistrito() {
    const url = `${this.api}resumen_pedido_distrito`;
    return this.http.get(url);
  }
  filtroResumenPedidoDistrito(data) {
    const url = `${this.api}filtro_resumen_pedido_distrito`;
    return this.http.post(url, data);
  }
  resumenPedidoRepartidor() {
    const url = `${this.api}resumen_pedido_repartidor`;
    return this.http.get(url);
  }
  filtroResumenPedidoRepartidor(data) {
    const url = `${this.api}filtro_resumen_pedido_repartidor`;
    return this.http.post(url, data);
  }
  getRepartidores() {
    const url = `${this.api}repartidores`;
    return this.http.get(url);
  }
  asignarRepartidor(data) {
    const url = `${this.api}asignarRepartidor`;
    return this.http.post(url, data);
  }
  getClientes() {
    const url = `${this.api}lista_cuy_cliente`;
    return this.http.get(url);
  }
  detallePedido(data) {
    const url = `${this.api}detalle_pedido_cliente`;
    return this.http.post(url, data);
  }
  detalleCliente(data) {
    const url = `${this.api}detalle_cuy_cliente`;
    return this.http.post(url, data);
  }
  validarDni(data) {
    const url = `${this.api}validarDni`;
    return this.http.post(url, data);
  }
  registerCliente(data) {
    const url = `${this.api}cliente`;
    return this.http.post(url, data);
  }
  updateCliente(data, id) {
    const url = `${this.api}cliente/${id}`;
    return this.http.put(url, data);
  }
  inicioReparto(data) {
    const url = `${this.api}inicioReparto`;
    return this.http.post(url, data);
  }
  getReparto() {
    const url = `${this.api}reparto`;
    return this.http.get(url);
  }
  registrarPago(data, id) {
    const url = `${this.api}registrarPago/${id}`;
    return this.http.post(url, data);
  }
  registrarEntrega(data, id) {
    const url = `${this.api}registrarEntrega/${id}`;
    return this.http.post(url, data);
  }
  detallePedidoRepartidor(data) {
    const url = `${this.api}detalle_pedido_repartidor`;
    return this.http.post(url, data);
  }
  detalleEntregaRepartidor(data) {
    const url = `${this.api}detalle_entrega_repartidor`;
    return this.http.post(url, data);
  }
  registrarNoEntrega(data, id) {
    const url = `${this.api}registrarNoEntrega/${id}`;
    return this.http.post(url, data);
  }
  userDestroy(id) {
    const url = `${this.api}user/${id}`;
    return this.http.delete(url);
  }
  habilitarUser(id) {
    const url = `${this.api}habilitarUser/${id}`;
    return this.http.get(url);
  }
}
