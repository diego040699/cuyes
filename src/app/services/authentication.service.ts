import { Injectable } from '@angular/core';
import { environment } from "../../environments/environment";
import { BehaviorSubject } from 'rxjs';
import { Platform } from '@ionic/angular';
import { HttpClient, HttpParams, HttpHeaders } from "@angular/common/http";
import { tap, catchError } from "rxjs/operators";
import { NavController, ToastController } from "@ionic/angular";
import { LoadingController } from "@ionic/angular";
import { Storage } from '@ionic/storage';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  authenticationState = new BehaviorSubject(false);
  private api = environment.api
  user = null;
  loaderToShow: any;
  constructor(
    private storage: Storage,
    private plt: Platform,
    private httpClient: HttpClient,
    private navCtrl: NavController,
    public loadingController: LoadingController,
    private toastCtrl: ToastController,
  ) {
    this.plt.ready().then(() => {
      this.checkToken();
    });
  }

  validarLogin(data) {
    this.showLoader();
    const url = `${this.api}login`;

    return this.httpClient.post(url, data).subscribe(res => {
      if (res["data"]) {
        this.user = res["data"];
        this.storage.set('USER_DATA', this.user);        
        this.authenticationState.next(true);
        const tipo_user = res["data"]["user"]["id_tipousuario"];
        if (tipo_user == 4) {
          this.navCtrl.navigateRoot("home");
        } else if (tipo_user == 5) {
          this.navCtrl.navigateRoot("home-vendedor");
        } else if (tipo_user == 2) {
          this.navCtrl.navigateRoot("home-repartidor");
        }
      } else {
        this.presentToast(res["message"]);
      }
      this.hideLoader();
    });

  }

  async presentToast(msg) {
    const toast = await this.toastCtrl.create({
      message: msg,
      duration: 2000
    });
    toast.present();
  }

  logout() {
    this.storage.remove("USER_DATA").then(() => {
      this.storage.set('CART_DATA', []);
      this.storage.set('DISTRITO_DATA', []);
      this.navCtrl.navigateRoot("login");
    });
  }

  isAuthenticated() {
    return this.authenticationState.value;
  }

  checkToken() {
    return this.storage.get('USER_DATA').then(res => {
      if (res) {
        this.authenticationState.next(true);
      }
    });
  }

  showLoader() {
    this.loaderToShow = this.loadingController
      .create({
        message: "Cargando..."
      })
      .then(res => {
        res.present();
        res.onDidDismiss().then(dis => { });
      });
  }

  hideLoader() {
    this.loadingController.dismiss();
  }

  getUser() {
    return new Promise((resolve, reject) => {
      this.storage
        .get("USER_DATA")
        .then(tokenValue => {
          resolve(tokenValue);
        })
        .catch(error => {
          reject(error);
        });
    });
  }

}
