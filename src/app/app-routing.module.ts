import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from './services/auth-guard.service';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  {
    path: 'home',
    loadChildren: () => import('./pages/cliente/home/home.module').then(m => m.HomePageModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./pages/auth/login/login.module').then(m => m.LoginPageModule)
  },
  {
    path: 'register-usuario',
    loadChildren: () => import('./pages/auth/register-usuario/register-usuario.module').then(m => m.RegisterUsuarioPageModule)
  },
  {
    path: 'producto',
    loadChildren: () => import('./pages/cliente/producto/producto.module').then(m => m.ProductoPageModule)
  },
  {
    path: 'carrito',
    loadChildren: () => import('./pages/cliente/carrito/carrito.module').then(m => m.CarritoPageModule)
  },
  {
    path: 'confirmar-pedido',
    loadChildren: () => import('./pages/cliente/confirmar-pedido/confirmar-pedido.module').then(m => m.ConfirmarPedidoPageModule)
  },
  {
    path: 'historial',
    loadChildren: () => import('./pages/cliente/historial/historial.module').then(m => m.HistorialPageModule)
  },
  {
    path: 'historial-detalle',
    loadChildren: () => import('./pages/cliente/historial-detalle/historial-detalle.module').then(m => m.HistorialDetallePageModule)
  },
  {
    path: 'perfil-cliente',
    loadChildren: () => import('./pages/cliente/perfil/perfil.module').then(m => m.PerfilPageModule)
  },
  {
    path: 'home-vendedor',
    loadChildren: () => import('./pages/vendedor/home-vendedor/home-vendedor.module').then(m => m.HomeVendedorPageModule)
  },
  {
    path: 'stock',
    loadChildren: () => import('./pages/vendedor/stock/stock.module').then(m => m.StockPageModule)
  },
  {
    path: 'pedidos-resumen',
    loadChildren: () => import('./pages/vendedor/pedidos-resumen/pedidos-resumen.module').then(m => m.PedidosResumenPageModule)
  },
  {
    path: 'pedidos-detalle',
    loadChildren: () => import('./pages/vendedor/pedidos-detalle/pedidos-detalle.module').then(m => m.PedidosDetallePageModule)
  },
  {
    path: 'cliente',
    loadChildren: () => import('./pages/vendedor/cliente/cliente.module').then(m => m.ClientePageModule)
  },
  {
    path: 'inicio-reparto',
    loadChildren: () => import('./pages/vendedor/inicio-reparto/inicio-reparto.module').then(m => m.InicioRepartoPageModule)
  },
  {
    path: 'cliente-detalle',
    loadChildren: () => import('./pages/vendedor/cliente-detalle/cliente-detalle.module').then(m => m.ClienteDetallePageModule)
  },
  {
    path: 'home-repartidor',
    loadChildren: () => import('./pages/repartidor/home-repartidor/home-repartidor.module').then(m => m.HomeRepartidorPageModule)
  },
  {
    path: 'detalle-pedido',
    loadChildren: () => import('./pages/repartidor/detalle-pedido/detalle-pedido.module').then(m => m.DetallePedidoPageModule)
  },
  {
    path: 'reset',
    loadChildren: () => import('./pages/auth/reset/reset.module').then(m => m.ResetPageModule)
  },
  {
    path: 'datos-comerciales',
    loadChildren: () => import('./pages/cliente/datos-comerciales/datos-comerciales.module').then( m => m.DatosComercialesPageModule)
  },
  {
    path: 'register-entrega',
    loadChildren: () => import('./pages/vendedor/register-entrega/register-entrega.module').then( m => m.RegisterEntregaPageModule)
  },
  {
    path: 'register-pago',
    loadChildren: () => import('./pages/vendedor/register-pago/register-pago.module').then( m => m.RegisterPagoPageModule)
  },
  {
    path: 'asignar-repartidor',
    loadChildren: () => import('./pages/vendedor/asignar-repartidor/asignar-repartidor.module').then( m => m.AsignarRepartidorPageModule)
  },
  {
    path: 'cliente-register',
    loadChildren: () => import('./pages/vendedor/cliente-register/cliente-register.module').then( m => m.ClienteRegisterPageModule)
  },
  {
    path: 'registrar-entrega',
    loadChildren: () => import('./pages/repartidor/registrar-entrega/registrar-entrega.module').then( m => m.RegistrarEntregaPageModule)
  },
  {
    path: 'no-recibido',
    loadChildren: () => import('./pages/repartidor/no-recibido/no-recibido.module').then( m => m.NoRecibidoPageModule)
  },

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
