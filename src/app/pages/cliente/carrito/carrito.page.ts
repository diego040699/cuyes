import { Component, OnInit } from '@angular/core';
import { MenuController, NavController, ToastController } from '@ionic/angular';
import { AppComponent } from './../../../app.component';
import { ServiciosService } from './../../../services/servicios.service';
import { LoadingController } from "@ionic/angular";
import { Storage } from '@ionic/storage';
import { formatDate } from '@angular/common';
import { Clipboard } from '@ionic-native/clipboard/ngx';

@Component({
  selector: 'app-carrito',
  templateUrl: './carrito.page.html',
  styleUrls: ['./carrito.page.scss'],
})
export class CarritoPage implements OnInit {

  carrito: any = [];
  medios: any = [];
  total = 0;
  count_cart = 0;
  costo_delivery = 0;
  cuypuntos = 0;
  fecha_entrega: String = new Date().toISOString();
  medio_pago;
  numero_cuenta = "Ninguno";

  constructor(
    private menu: MenuController,
    private navCtrl: NavController,
    private appComponent: AppComponent,
    public loadingController: LoadingController,
    private service: ServiciosService,
    private storage: Storage,
    private toastCtrl: ToastController,
    private clipboard: Clipboard
  ) {
    this.menu.enable(true);
  }

  copiarNumero() {
    this.clipboard.copy(this.numero_cuenta);
    this.presentToast("Numero de cuenta copiado","success");
  }
  setNumCuenta() {
    this.medios.forEach(element => {
      if (element["id"] == this.medio_pago) {
        this.numero_cuenta = element["numero"];
      }
    });
  }

  getCostoDelivery() {
    this.storage.get('USER_DATA').then(res_user => {
      if (res_user) {
        this.costo_delivery = res_user["cliente"]["grupo_cliente"]["costo_delivery"];
      }
    });
  }

  countCart() {
    this.storage.get('CART_DATA').then(res => {
      this.count_cart = res.length;
    });
  }

  goToPages(name) {
    this.navCtrl.navigateRoot(name);
  }

  pedido() {
    if (this.medio_pago) {
      this.storage.get('USER_DATA').then(res_user => {
        const data = {
          "pedido": {
            "id_cliente": res_user["cliente"]["id"],
            "precio_delivery": parseFloat(this.costo_delivery.toFixed(2)),
            "id_medio": parseInt(this.medio_pago),
            "fecha_entrega": formatDate(this.fecha_entrega.toString(), 'yyyy-MM-dd', 'en-ES'),
            "total": parseFloat(this.total.toFixed(2))
          },
          "data_productos": this.carrito,
          "data_cliente": {},
          "cuy_puntos": this.cuypuntos
        };
        this.storage.set('PEDIDO_DATA', data);
        this.navCtrl.navigateRoot('confirmar-pedido');
      });
    } else {
      this.presentToast("Seleccione medio de pago", 'danger');
    }
  }

  getCart() {
    this.storage.get('CART_DATA').then(res => {
      this.carrito = res;
      this.montoTotal();
    });
  }

  montoTotal() {
    this.total = 0;
    this.cuypuntos = 0;
    this.carrito.forEach(element => {
      this.total += parseFloat(element["subtotal"]);
      this.cuypuntos += element["cantidad"];
    });
  }

  addCantidad(index) {
    this.carrito.forEach((element, i) => {
      if (index == i) {
        if (element["producto"]["stock"] > element["cantidad"]) {
          element["cantidad"] = element["cantidad"] + 1;
          element['subtotal'] = (element['cantidad'] * element['monto']).toFixed(2);
        } else {
          this.presentToast("Stock insuficiente", 'danger');
        }
      }
    });
    this.storage.set('CART_DATA', this.carrito);
    this.montoTotal();
  }

  removeCantidad(index) {
    this.carrito.forEach((element, i) => {
      if (index == i) {
        if (element["cantidad"] > 1) {
          element["cantidad"] = element["cantidad"] - 1;
          element['subtotal'] = (element['cantidad'] * element['monto']).toFixed(2);
        }
      }
    });
    this.storage.set('CART_DATA', this.carrito);
    this.montoTotal();
  }

  removeProducto(index) {
    this.carrito.forEach((element, i) => {
      if (index == i) {
        this.carrito.splice(this.carrito.indexOf(element), 1);
      }
    });
    this.storage.set('CART_DATA', this.carrito);
    this.montoTotal();
    this.count_cart = this.carrito.length;
    this.presentToast('Producto Eliminado', 'danger');
  }

  async presentToast(msg, color) {
    const toast = await this.toastCtrl.create({
      color: color,
      message: msg,
      duration: 2000
    });
    toast.present();
  }

  getMedios() {
    this.service.medios().subscribe(res => {
      this.medios = res["data"];
    });
  }

  ngOnInit() {
    this.getCart();
    this.countCart();
    this.getCostoDelivery();
    this.getMedios();
  }

}
