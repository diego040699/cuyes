import { Component, OnInit } from '@angular/core';
import { AlertController, MenuController, NavController, ToastController } from '@ionic/angular';
import { AppComponent } from './../../../app.component';
import { ServiciosService } from './../../../services/servicios.service';
import { LoadingController } from "@ionic/angular";
import { Storage } from '@ionic/storage';
import { ActivatedRoute } from '@angular/router';
import { Clipboard } from '@ionic-native/clipboard/ngx';

@Component({
  selector: 'app-historial-detalle',
  templateUrl: './historial-detalle.page.html',
  styleUrls: ['./historial-detalle.page.scss'],
})
export class HistorialDetallePage implements OnInit {

  loaderToShow: any;
  productos: any = [];
  pedido = {};
  constructor(
    private menu: MenuController,
    private navCtrl: NavController,
    private appComponent: AppComponent,
    public loadingController: LoadingController,
    private service: ServiciosService,
    private storage: Storage,
    private route: ActivatedRoute,
    private alertCtrl: AlertController,
    private clipboard: Clipboard,
    private toastCtrl: ToastController
  ) {
    this.menu.enable(true);
  }

  back() {
    this.navCtrl.navigateRoot('historial');
  }

  goToPage(page) {
    this.navCtrl.navigateRoot(page);
  }

  getPedido() {
    this.route.queryParamMap.subscribe(result => {
      this.service.getPedido(result['params']["id"]).subscribe(res => {
        console.log(res["data"])
        this.pedido = res["data"]["data_pedido"];
        this.productos = res["data"]["data_productos"];
      });
    });
  }

  copiarNumero() {
    this.clipboard.copy(this.pedido["numero_cuenta"]);
    this.presentToast("Numero de cuenta copiado", "success");
  }

  async presentToast(msg, color) {
    const toast = await this.toastCtrl.create({
      color: color,
      message: msg,
      duration: 2000
    });
    toast.present();
  }

  async cancelarPedido() {
    const alert = await this.alertCtrl.create({
      header: '',
      message: '¿Esta seguro de cancelar este pedido?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
          }
        }, {
          text: 'Confirmar',
          handler: () => {
            this.showLoader();
            this.service.cancelarPedido(parseInt(this.pedido["id"])).subscribe(res => {
              this.hideLoader();
              this.alerta(res["message"]);
            });
          }
        }
      ]
    });
    await alert.present();
  }

  alerta(message) {
    let alert = this.alertCtrl.create({
      message: message,
      buttons: [
        {
          text: 'Ok',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            this.navCtrl.navigateRoot('historial?id=1');
          }
        }
      ]
    });
    alert.then(alert => alert.present());
  }

  showLoader() {
    this.loaderToShow = this.loadingController
      .create({
        message: "Cargando..."
      })
      .then(res => {
        res.present();
        res.onDidDismiss().then(dis => { });
      });
  }

  hideLoader() {
    this.loadingController.dismiss();
  }

  ngOnInit() {
    this.getPedido();
  }

}
