import { Component } from '@angular/core';
import { MenuController, NavController, ToastController } from '@ionic/angular';
import { AppComponent } from './../../../app.component';
import { ServiciosService } from './../../../services/servicios.service';
import { LoadingController } from "@ionic/angular";
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  loaderToShow: any;
  productos: any = [];
  count_cart = 0;
  name = "";
  contentLoaded = false;

  constructor(
    private menu: MenuController,
    private navCtrl: NavController,
    private appComponent: AppComponent,
    public loadingController: LoadingController,
    private service: ServiciosService,
    private storage: Storage,
    private router: Router,
    private toastCtrl: ToastController,
  ) {
    this.menu.enable(true);
  }

  countCart() {
    this.storage.get('CART_DATA').then(res => {
      this.count_cart = res.length;
    });
  }

  getName() {
    this.storage.get('USER_DATA').then(res => {
      if (res) {
        this.name = res["user"]["name"];
        const names = this.name.split(" ");
        this.name = names[0];
      }
    });
  }

  getProductos() {
    this.storage.get('CART_DATA').then(res => {
      if (!res) {
        this.storage.set('CART_DATA', []);
      }
    });
    this.storage.get('USER_DATA').then(res => {
      var data = {};
      if (res) {
        data = { id_grupo_cliente: res["cliente"]["id_grupocliente"] };
      } else {
        data = { id_grupo_cliente: 1 };
      }
      this.service.listProductos(data)
        .subscribe(res => {
          this.contentLoaded = true;
          this.productos = res["data"];
        });
    });
  }

  goToPage(num) {
    let page = this.appComponent.appPages[num];
    this.appComponent.openPageRoot(page);
  }

  goToPages(name) {
    this.navCtrl.navigateRoot(name);
  }

  showLoader() {
    this.loaderToShow = this.loadingController
      .create({
        message: "Cargando..."
      })
      .then(res => {
        res.present();
        res.onDidDismiss().then(dis => { });
      });
  }

  hideLoader() {
    this.loadingController.dismiss();
  }

  async presentToast(msg) {
    const toast = await this.toastCtrl.create({
      color: 'danger',
      message: msg,
      duration: 2000
    });
    toast.present();
  }

  carrito() {
    this.navCtrl.navigateForward('carrito');
  }

  detalle(page, stock) {
    this.navCtrl.navigateForward(page);
    // if (stock > 0) {
    // } else {
    //   this.presentToast("Stock Agotado");
    // }
  }

  ngOnInit() {
    // this.showLoader();
    this.appComponent.setTipoUser();
    this.getName();
    this.getProductos();
    this.countCart();
  }

  ionViewWillEnter() {
    this.getProductos();
  }

}
