import { Component, OnInit } from '@angular/core';
import { MenuController, NavController } from '@ionic/angular';
import { ServiciosService } from './../../../services/servicios.service';
import { LoadingController } from "@ionic/angular";
import { Storage } from '@ionic/storage';
import { formatDate } from '@angular/common';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-historial',
  templateUrl: './historial.page.html',
  styleUrls: ['./historial.page.scss'],
})
export class HistorialPage implements OnInit {

  cuypuntos = 0;
  historial: any = [];
  estados: any = [];
  estado: Number = 0;
  fecha: String = new Date().toISOString();

  constructor(
    private menu: MenuController,
    private navCtrl: NavController,
    public loadingController: LoadingController,
    private service: ServiciosService,
    private storage: Storage,
    private route: ActivatedRoute
  ) {
    this.menu.enable(true);
  }

  getUser() {
    this.storage.get('USER_DATA').then(res => {
      this.service.getUser(res["cliente"]["id"]).subscribe(res => {
        this.cuypuntos = res["data"]["cuy_puntos"];
      });
    });
  }

  getHistorial() {
    this.storage.get('USER_DATA').then(res => {
      const data = { "id_cliente": res["cliente"]["id"] };
      this.service.getHistorial(data).subscribe(res => {
        this.historial = res["data"];
      });
    });
  }

  historialEstado() {
    this.storage.get('USER_DATA').then(res => {
      const data = {
        "id_cliente": res["cliente"]["id"],
        "estado": this.estado
      };

      this.service.filtro_historial_estado(data).subscribe(res => {
        this.historial = res["data"];
      });
    });
  }

  getEstados() {
    this.service.getEstados().subscribe(res => {
      this.estados = res["data"];
    });
  }

  goToPage(page) {
    this.navCtrl.navigateForward(page);
  }

  historialFecha() {
    this.storage.get('USER_DATA').then(res => {
      const data = {
        "id_cliente": res["cliente"]["id"],
        "fecha": formatDate(this.fecha.toString(), 'yyyy-MM-dd', 'en-ES')
      };

      this.service.filtro_historial_fecha(data).subscribe(res => {
        this.historial = res["data"];
      });
    });
  }


  ngOnInit() {
    this.getUser();
    this.getEstados();
    this.getHistorial();
    this.route.queryParamMap.subscribe(res => {
      if (res['params']) {
        this.getHistorial();
      }
    });
  }

}
