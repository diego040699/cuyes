import { Component, OnInit } from '@angular/core';
import { AlertController, MenuController, NavController, ToastController } from '@ionic/angular';
import { AppComponent } from './../../../app.component';
import { ServiciosService } from './../../../services/servicios.service';
import { LoadingController } from "@ionic/angular";
import { Storage } from '@ionic/storage';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-producto',
  templateUrl: './producto.page.html',
  styleUrls: ['./producto.page.scss'],
})
export class ProductoPage implements OnInit {

  producto: any = [];
  cantidad = 1;
  count_cart = 0;
  costo_delivery = 0;
  delivery_gratis = 0;

  constructor(
    private menu: MenuController,
    private navCtrl: NavController,
    private appComponent: AppComponent,
    public loadingController: LoadingController,
    private service: ServiciosService,
    private storage: Storage,
    private route: ActivatedRoute,
    private alertCtrl: AlertController,
    private toastCtrl: ToastController
  ) {
    this.menu.enable(true);
  }

  goToPages(name) {
    this.navCtrl.navigateRoot(name);
  }

  countCart() {
    this.storage.get('CART_DATA').then(res => {
      this.count_cart = res.length;
    });
  }

  back() {
    this.navCtrl.pop();
  }

  async login() {
    const alert = await this.alertCtrl.create({
      header: '',
      message: 'Antes de agregar su producto al carrito deberá registrarse',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
          }
        }, {
          text: 'Aceptar',
          handler: () => {
            this.navCtrl.navigateRoot('login');
          }
        }
      ]
    });
    await alert.present();
  }

  addCart() {
    this.storage.get('USER_DATA').then(res => {
      if (res) {
        var monto = 0;
        if (this.producto['precio_oferta']) {
          monto = parseFloat(this.producto['precio_oferta']);
        } else {
          monto = parseFloat(this.producto['precio']);
        }

        this.storage.get('CART_DATA').then(res => {
          var cart = {
            producto: this.producto,
            cantidad: this.cantidad,
            monto: monto.toFixed(2),
            subtotal: (this.cantidad * monto).toFixed(2)
          }

          var carrito = res;
          var c = 0;
          carrito.forEach(item => {
            if (item['producto']['id'] == this.producto['id']) {
              if (item['cantidad'] + this.cantidad <= item['producto']['stock']) {
                item['cantidad'] = item['cantidad'] + this.cantidad;
                item['subtotal'] = item['cantidad'] * item['monto'];
                c = 1;
              } else {
                c = 2;
              }
            }
          });
          if (c == 0) {
            carrito.push(cart);
            this.storage.set('CART_DATA', carrito);
            this.showAlert();
          } else if (c == 1) {
            this.storage.set('CART_DATA', carrito);
            this.showAlert();
          } else if (c == 2) {
            this.presentToast("Stock insuficiente");
          }
        });
      } else {
        this.login();
      }
    });
  }

  showAlert() {
    this.navCtrl.navigateRoot('carrito');
    // let alert = this.alertCtrl.create({
    //   message: "Producto agregado al carrito",
    //   buttons: [
    //     {
    //       text: 'Aceptar',
    //       handler: () => {
    //         this.navCtrl.navigateRoot('carrito');
    //       }
    //     }
    //   ]
    // });
    // alert.then(alert => alert.present());
  }

  getProducto() {
    this.storage.get('USER_DATA').then(res_user => {
      var id_grupo_cliente = 1;
      if (res_user) {
        id_grupo_cliente = res_user["cliente"]["id_grupocliente"];
        this.costo_delivery = res_user["cliente"]["grupo_cliente"]["costo_delivery"];
        this.delivery_gratis = res_user["cliente"]["grupo_cliente"]["delivery_gratis"];
      }
      this.route.queryParamMap.subscribe(result => {
        const data = { id: result['params']["id"], id_grupo_cliente: id_grupo_cliente };
        this.service.detalleProducto(data).subscribe(res => {
          this.producto = res["data"];
        });
      });
    });
  }

  addCantidad() {
    if (this.cantidad < this.producto["stock"]) {
      this.cantidad++;
    } else {
      this.presentToast("Stock insuficiente");
    }
  }

  removeCantidad() {
    if (this.cantidad > 1) {
      this.cantidad = this.cantidad - 1;
    }
  }

  async presentToast(msg) {
    const toast = await this.toastCtrl.create({
      color: 'danger',
      message: msg,
      duration: 2000
    });
    toast.present();
  }

  ngOnInit() {
    this.getProducto();
    this.countCart();
  }

}
