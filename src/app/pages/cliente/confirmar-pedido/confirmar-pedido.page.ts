import { Component, OnInit, ViewChild } from '@angular/core';
import { AlertController, MenuController, NavController } from '@ionic/angular';
import { AppComponent } from './../../../app.component';
import { ServiciosService } from './../../../services/servicios.service';
import { LoadingController } from "@ionic/angular";
import { Storage } from '@ionic/storage';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

declare var google;

@Component({
  selector: 'app-confirmar-pedido',
  templateUrl: './confirmar-pedido.page.html',
  styleUrls: ['./confirmar-pedido.page.scss'],
})
export class ConfirmarPedidoPage implements OnInit {

  public fGroup: FormGroup;
  loaderToShow: any;
  distritos: any = [];
  map = null;
  marker = null;
  geoCoder = null;
  visibilityMap = false;

  constructor(
    private menu: MenuController,
    private navCtrl: NavController,
    private appComponent: AppComponent,
    public loadingController: LoadingController,
    private service: ServiciosService,
    private storage: Storage,
    private geolocation: Geolocation,
    private fBuilder: FormBuilder,
    private alertCtrl: AlertController
  ) {
    this.menu.enable(true);
  }

  goToPage(page) {
    this.navCtrl.navigateRoot(page);
  }

  back() {
    this.navCtrl.navigateRoot('carrito');
  }

  /*Geolocalizacion*/

  async loadMap() {
    this.visibilityMap = true;
    var myLatLng;
    if (this.fGroup.value.lat && this.fGroup.value.lng) {
      myLatLng = {
        lat: this.fGroup.value.lat,
        lng: this.fGroup.value.lng
      };
      window.setTimeout(() => {
        this.setMap(myLatLng);
      }, 100);
    } else {
      myLatLng = await this.getLocation();
      window.setTimeout(() => {
        this.setMap(myLatLng);
      }, 100);
    }
  }

  setMap(myLatLng) {
    this.geoCoder = new google.maps.Geocoder();
    const mapEle: HTMLElement = document.getElementById('map');
    this.map = new google.maps.Map(mapEle, {
      center: myLatLng,
      zoom: 12
    });

    google.maps.event.addListenerOnce(this.map, 'idle', () => {
      this.geocodePosition(myLatLng);
      this.addMarker(myLatLng.lat, myLatLng.lng);
      google.maps.event.addListener(this.marker, 'dragend', () => {
        this.geocodePosition(this.marker.getPosition());
      });
    });
  }

  geocodePosition(myLatLng) {
    this.geoCoder.geocode({
      latLng: myLatLng
    }, (responses) => {
      if (responses && responses.length > 0) {
        this.fGroup.get('direccion').setValue(responses[0].formatted_address);
        this.fGroup.get('lat').setValue(this.marker.position.lat());
        this.fGroup.get('lng').setValue(this.marker.position.lng());
        // console.log(responses[0].formatted_address);
        // console.log("lat: " + this.marker.position.lat());
        // console.log("lng: " + this.marker.position.lng());
      } else {

      }
    });
  }
  private addMarker(lat: number, lng: number) {
    this.marker = new google.maps.Marker({
      draggable: true,
      position: { lat, lng },
      map: this.map,
      title: 'Hello mundo'
    });
  }

  private async getLocation() {
    const res = await this.geolocation.getCurrentPosition();
    return {
      lat: res.coords.latitude,
      lng: res.coords.longitude
    };
  }

  /*Fin Geolocalizacion*/

  showLoader() {
    this.loaderToShow = this.loadingController
      .create({
        message: "Cargando..."
      })
      .then(res => {
        res.present();
        res.onDidDismiss().then(dis => { });
      });
  }

  hideLoader() {
    this.loadingController.dismiss();
  }

  save() {
    if (this.fGroup.value.lat) {
      this.storage.get('PEDIDO_DATA').then(res => {
        var pedido = res;
        pedido["data_cliente"] = this.fGroup.value;
        this.showLoader();
        this.service.pedidoRegister(pedido).subscribe(res => {
          this.hideLoader();
          this.storage.set('USER_DATA', res["data"]);
          this.storage.set('PEDIDO_DATA', {});
          this.storage.set('CART_DATA', []);
          this.showAlert(res["message"]);
        });
      });
    } else {
      this.showAlert2("Por favor activar GPS");
    }
  }

  async showAlert2(message) {
    const alert = await this.alertCtrl.create({
      header: '',
      message: message,
      buttons: [
        {
          text: 'Aceptar'
        }
      ]
    });
    await alert.present();
  }

  getUser() {
    this.storage.get('USER_DATA').then(res => {
      this.fGroup.get('id_cliente').setValue(res["cliente"]["id"]);
      this.fGroup.get('dni').setValue(res["user"]["dni"]);
      this.fGroup.get('name').setValue(res["user"]["name"]);
      this.fGroup.get('recibe').setValue(res["user"]["name"]);
      this.fGroup.get('distrito').setValue(res["cliente"]["distrito"]);
      this.fGroup.get('direccion').setValue(res["cliente"]["direccion"]);
      this.fGroup.get('celular').setValue(res["user"]["celular"]);
      this.fGroup.get('wsp').setValue(res["user"]["wsp"]);
      this.fGroup.get('lat').setValue(parseFloat(res["cliente"]["lat"]));
      this.fGroup.get('lng').setValue(parseFloat(res["cliente"]["lng"]));
    });
  }

  getDistritos() {
    this.service.getDistritos().subscribe(res => {
      this.distritos = res["data"];
      this.getUser();
    });
  }

  async showAlert(message) {
    const alert = await this.alertCtrl.create({
      header: '',
      message: message,
      buttons: [
        {
          text: 'Aceptar',
          handler: () => {
            this.navCtrl.navigateRoot('home');
          }
        }
      ]
    });
    await alert.present();
  }

  ngOnInit() {
    this.fGroup = this.fBuilder.group({
      id_cliente: [0, [Validators.required]],
      dni: ["", [Validators.required, Validators.maxLength(8), Validators.minLength(8)]],
      name: ["", [Validators.required]],
      distrito: ["", [Validators.required]],
      lat: ["", [Validators.required]],
      lng: ["", [Validators.required]],
      direccion: ["", [Validators.required]],
      celular: ["", [Validators.required, Validators.maxLength(9), Validators.minLength(9)]],
      wsp: ["", [Validators.required, Validators.maxLength(9), Validators.minLength(9)]],
      recibe: ["", [Validators.required]]
    });
    this.getDistritos();
  }

}
