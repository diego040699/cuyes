import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DatosComercialesPage } from './datos-comerciales.page';

const routes: Routes = [
  {
    path: '',
    component: DatosComercialesPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DatosComercialesPageRoutingModule {}
