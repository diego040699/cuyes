import { Component, OnInit } from '@angular/core';
import { MenuController, NavController } from '@ionic/angular';
import { ServiciosService } from './../../../services/servicios.service';
import { LoadingController } from "@ionic/angular";
import { Storage } from '@ionic/storage';
import { formatDate } from '@angular/common';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-datos-comerciales',
  templateUrl: './datos-comerciales.page.html',
  styleUrls: ['./datos-comerciales.page.scss'],
})
export class DatosComercialesPage implements OnInit {

  info = {};

  constructor(
    private menu: MenuController,
    private navCtrl: NavController,
    public loadingController: LoadingController,
    private service: ServiciosService,
    private storage: Storage,
    private route: ActivatedRoute
  ) {
    this.menu.enable(true);
  }

  goToPage(page) {
    this.navCtrl.navigateForward(page);
  }

  getInfo() {
    this.service.getInformacion().subscribe(res => {
      this.info = res["data"];
    });
  }

  ngOnInit() {
    this.getInfo();
  }

}
