import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DatosComercialesPage } from './datos-comerciales.page';

describe('DatosComercialesPage', () => {
  let component: DatosComercialesPage;
  let fixture: ComponentFixture<DatosComercialesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DatosComercialesPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DatosComercialesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
