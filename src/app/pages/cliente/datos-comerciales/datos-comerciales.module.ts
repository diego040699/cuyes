import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DatosComercialesPageRoutingModule } from './datos-comerciales-routing.module';

import { DatosComercialesPage } from './datos-comerciales.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DatosComercialesPageRoutingModule
  ],
  declarations: [DatosComercialesPage]
})
export class DatosComercialesPageModule {}
