import { Component, OnInit } from '@angular/core';
import { MenuController, NavController } from '@ionic/angular';
import { AppComponent } from './../../../app.component';
import { ServiciosService } from './../../../services/servicios.service';
import { LoadingController } from "@ionic/angular";
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-home-repartidor',
  templateUrl: './home-repartidor.page.html',
  styleUrls: ['./home-repartidor.page.scss'],
})
export class HomeRepartidorPage implements OnInit {

  name = "";
  pedidos: any = [];

  constructor(
    private menu: MenuController,
    private navCtrl: NavController,
    private appComponent: AppComponent,
    public loadingController: LoadingController,
    private service: ServiciosService,
    private storage: Storage
  ) {
    this.menu.enable(true);
  }

  goToPage(page) {
    this.navCtrl.navigateForward(page);
  }

  getPedidos() {
    this.storage.get('USER_DATA').then(result => {
      const data = { id_repartidor: result['user']['id'] };
      this.service.detallePedidoRepartidor(data).subscribe(res => {
        this.pedidos = res["data"];
      });
    });
  }

  ngOnInit() {
    this.storage.get('USER_DATA').then(res => {
      this.name = res["user"]["name"];
      const names = this.name.split(" ");
      this.name = names[0];
    });
    this.appComponent.setTipoUser();
    this.getPedidos();
  }

  ionViewWillEnter() {
    this.getPedidos();
  }

}
