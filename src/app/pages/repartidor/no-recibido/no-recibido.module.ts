import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { NoRecibidoPageRoutingModule } from './no-recibido-routing.module';

import { NoRecibidoPage } from './no-recibido.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NoRecibidoPageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [NoRecibidoPage]
})
export class NoRecibidoPageModule { }
