import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NoRecibidoPage } from './no-recibido.page';

const routes: Routes = [
  {
    path: '',
    component: NoRecibidoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NoRecibidoPageRoutingModule {}
