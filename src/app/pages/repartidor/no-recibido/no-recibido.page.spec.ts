import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { NoRecibidoPage } from './no-recibido.page';

describe('NoRecibidoPage', () => {
  let component: NoRecibidoPage;
  let fixture: ComponentFixture<NoRecibidoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NoRecibidoPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(NoRecibidoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
