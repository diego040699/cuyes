import { Component, OnInit } from '@angular/core';
import { MenuController, NavController } from '@ionic/angular';
import { AppComponent } from './../../../app.component';
import { ServiciosService } from './../../../services/servicios.service';
import { LoadingController } from "@ionic/angular";
import { Storage } from '@ionic/storage';
import { ActivatedRoute } from '@angular/router';
import { Geolocation } from '@ionic-native/geolocation/ngx';

declare var google;

@Component({
  selector: 'app-detalle-pedido',
  templateUrl: './detalle-pedido.page.html',
  styleUrls: ['./detalle-pedido.page.scss'],
})
export class DetallePedidoPage implements OnInit {

  map = null;
  marker = null;
  geoCoder = null;
  visibilityMap = false;
  id_pedido = 0;
  detalle = {
    "data_cliente": {},
    "data_pedido": {},
    "data_productos": [],
    "data_repartidor": {}
  };

  constructor(
    private menu: MenuController,
    private navCtrl: NavController,
    private appComponent: AppComponent,
    public loadingController: LoadingController,
    private service: ServiciosService,
    private storage: Storage,
    private geolocation: Geolocation,
    private route: ActivatedRoute
  ) {
    this.menu.enable(true);
  }

  back() {
    this.navCtrl.pop();
  }

  goToPage(page) {
    this.navCtrl.navigateForward(page);
  }

  /*Geolocalizacion*/

  async loadMap() {
    this.visibilityMap = true;
    var myLatLng = await this.getLocation();
    this.setMap(myLatLng);
  }

  setMap(myLatLng) {
    this.geoCoder = new google.maps.Geocoder();
    const mapEle: HTMLElement = document.getElementById('map');
    if (mapEle) {
      this.map = new google.maps.Map(mapEle, {
        center: myLatLng,
        zoom: 14
      });

      google.maps.event.addListenerOnce(this.map, 'idle', () => {
        this.addMarker(myLatLng.lat, myLatLng.lng);
      });
    }
  }

  private addMarker(lat: number, lng: number) {
    this.marker = new google.maps.Marker({
      draggable: false,
      position: { lat, lng },
      map: this.map,
      title: 'Hello mundo'
    });
  }

  private async getLocation() {
    const res = await this.geolocation.getCurrentPosition();
    return {
      lat: this.detalle["data_cliente"]["latitud"],
      lng: this.detalle["data_cliente"]["longitud"]
    };
  }

  /*Fin Geolocalizacion*/

  detallePedido() {
    this.route.queryParamMap.subscribe(result => {
      this.id_pedido = result['params']["id"];
      const data = { "id_pedido": this.id_pedido };
      this.service.detallePedido(data).subscribe(res => {
        this.detalle = res["data"];
      });
    });
  }


  ngOnInit() {
    this.detallePedido();
  }

}
