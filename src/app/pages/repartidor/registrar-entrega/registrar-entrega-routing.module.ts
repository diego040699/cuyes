import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RegistrarEntregaPage } from './registrar-entrega.page';

const routes: Routes = [
  {
    path: '',
    component: RegistrarEntregaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RegistrarEntregaPageRoutingModule {}
