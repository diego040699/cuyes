import { Component, OnInit } from '@angular/core';
import { AlertController, MenuController, NavController } from '@ionic/angular';
import { AppComponent } from './../../../app.component';
import { ServiciosService } from './../../../services/servicios.service';
import { LoadingController } from "@ionic/angular";
import { Storage } from '@ionic/storage';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-registrar-entrega',
  templateUrl: './registrar-entrega.page.html',
  styleUrls: ['./registrar-entrega.page.scss'],
})
export class RegistrarEntregaPage implements OnInit {

  public fGroup: FormGroup;
  loaderToShow: any;
  base64img: string = '';
  medioPago: any = [];
  id_pedido = 0;
  detalle = {
    "data_cliente": {},
    "data_pedido": {},
    "data_productos": [],
    "data_repartidor": {}
  };

  constructor(
    private menu: MenuController,
    private navCtrl: NavController,
    private fBuilder: FormBuilder,
    private appComponent: AppComponent,
    public loadingController: LoadingController,
    private service: ServiciosService,
    private storage: Storage,
    public camera: Camera,
    private route: ActivatedRoute,
    private alertCtrl: AlertController
  ) {
    this.menu.enable(true);
  }

  back() {
    this.navCtrl.pop();
  }

  imageCaptured() {
    const options: CameraOptions = {
      quality: 70,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }
    this.camera.getPicture(options).then((ImageData => {
      this.base64img = "data:image/jpeg;base64," + ImageData;
      this.fGroup.get('foto').setValue(this.base64img);
    }), error => {
      console.log(error);
    })
  }

  imageCapturedGallery() {
    const options: CameraOptions = {
      quality: 70,
      destinationType: this.camera.DestinationType.DATA_URL,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      saveToPhotoAlbum: false
    }
    this.camera.getPicture(options).then((ImageData => {
      this.base64img = "data:image/jpeg;base64," + ImageData;
      this.fGroup.get('foto').setValue(this.base64img);
    }), error => {
      console.log(error);
    })
  }

  getMediosPago() {
    this.service.medios().subscribe(res => {
      this.medioPago = res["data"];
    })
  }

  detallePedido() {
    this.route.queryParamMap.subscribe(result => {
      this.id_pedido = result['params']["id"];
      const data = { "id_pedido": this.id_pedido };
      this.service.detallePedido(data).subscribe(res => {
        this.detalle = res["data"];
        this.fGroup.get('medio_pago').setValue(this.detalle['data_pedido']['id_medio']);
        console.log(res['data'])
      });
    });
  }

  save() {
    this.showLoader();
    this.service.registrarEntrega(this.fGroup.value, this.id_pedido).subscribe(res => {
      this.hideLoader();
      this.showAlert(res["message"]);
    });
  }

  async showAlert(message) {
    const alert = await this.alertCtrl.create({
      header: '',
      message: message,
      buttons: [
        {
          text: 'Aceptar',
          handler: () => {
            this.navCtrl.navigateRoot('detalle-pedido');
          }
        }
      ]
    });
    await alert.present();
  }

  showLoader() {
    this.loaderToShow = this.loadingController
      .create({
        message: "Cargando..."
      })
      .then(res => {
        res.present();
        res.onDidDismiss().then(dis => { });
      });
  }

  hideLoader() {
    this.loadingController.dismiss();
  }

  ngOnInit() {
    this.fGroup = this.fBuilder.group({
      foto: [""],
      medio_pago: [""],
      pago_entrega: [false]
    });
    this.getMediosPago();
    this.detallePedido();
  }

}
