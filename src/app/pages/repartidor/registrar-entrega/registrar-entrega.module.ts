import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RegistrarEntregaPageRoutingModule } from './registrar-entrega-routing.module';

import { RegistrarEntregaPage } from './registrar-entrega.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RegistrarEntregaPageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [RegistrarEntregaPage]
})
export class RegistrarEntregaPageModule { }
