import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PedidosResumenPageRoutingModule } from './pedidos-resumen-routing.module';

import { PedidosResumenPage } from './pedidos-resumen.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PedidosResumenPageRoutingModule
  ],
  declarations: [PedidosResumenPage]
})
export class PedidosResumenPageModule {}
