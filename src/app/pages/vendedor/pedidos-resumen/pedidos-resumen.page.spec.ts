import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PedidosResumenPage } from './pedidos-resumen.page';

describe('PedidosResumenPage', () => {
  let component: PedidosResumenPage;
  let fixture: ComponentFixture<PedidosResumenPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PedidosResumenPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PedidosResumenPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
