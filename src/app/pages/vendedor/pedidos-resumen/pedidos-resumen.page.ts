import { Component, OnInit } from '@angular/core';
import { MenuController, NavController, ToastController } from '@ionic/angular';
import { AppComponent } from './../../../app.component';
import { ServiciosService } from './../../../services/servicios.service';
import { LoadingController } from "@ionic/angular";
import { Storage } from '@ionic/storage';
import { formatDate } from '@angular/common';

@Component({
  selector: 'app-pedidos-resumen',
  templateUrl: './pedidos-resumen.page.html',
  styleUrls: ['./pedidos-resumen.page.scss'],
})
export class PedidosResumenPage implements OnInit {

  segment = 1;
  resumen: any = [];
  resumenCategoria: any = [];
  resumenDistrito: any = [];
  resumenRepartidor: any = [];
  fechaResumen;
  fechaResumenCategoria;
  fechaResumenDistrito;
  fechaResumenRepartidor;

  constructor(
    private menu: MenuController,
    private navCtrl: NavController,
    private appComponent: AppComponent,
    public loadingController: LoadingController,
    private service: ServiciosService,
    private storage: Storage,
    private toastCtrl: ToastController
  ) {
    this.menu.enable(true);
  }

  goToPage(page) {
    this.navCtrl.navigateForward(page);
  }

  compartirWsp() {

  }

  setDistrito(id, nombre) {
    this.storage.get('DISTRITO_DATA').then(res => {
      const distritos = res;
      var c = 0;
      const new_distrito = {
        "id": id,
        "nombre": nombre
      };

      distritos.forEach(item => {
        (item['id'] == id) ? c = 1 : '';
      });
      if (c == 1) {
        this.presentToast("Distrito ya ha sido agregado", "danger");
      } else {
        distritos.push(new_distrito);
        this.storage.set('DISTRITO_DATA', distritos);
        this.presentToast("Distrito agregado", "success");
      }
    });
  }

  async presentToast(msg, color) {
    const toast = await this.toastCtrl.create({
      color: color,
      message: msg,
      duration: 2000
    });
    toast.present();
  }

  resumenPedidoCliente() {
    this.service.resumenPedidoCliente().subscribe(res => {
      this.resumen = res["data"];
    });
  }

  resumenPedidoCategoria() {
    this.service.resumenPedidoCategoria().subscribe(res => {
      this.resumenCategoria = res["data"];
    });
  }

  resumenPedidoDistrito() {
    this.service.resumenPedidoDistrito().subscribe(res => {
      this.resumenDistrito = res["data"];
    });
  }

  resumenPedidoRepartidor() {
    this.service.resumenPedidoRepartidor().subscribe(res => {
      this.resumenRepartidor = res["data"];
    });
  }

  /*Filtros*/

  resumenPedidoClienteFilter() {
    const data = { "fecha": formatDate(this.fechaResumen.toString(), 'yyyy-MM-dd', 'en-ES') };
    this.service.filtroResumenPedidoCliente(data).subscribe(res => {
      this.resumen = res["data"];
    });
  }

  resumenPedidoCategoriaFilter() {
    const data = { "fecha": formatDate(this.fechaResumenCategoria.toString(), 'yyyy-MM-dd', 'en-ES') };
    this.service.filtroResumenPedidoCategoria(data).subscribe(res => {
      this.resumenCategoria = res["data"];
    });
  }

  resumenPedidoDistritoFilter() {
    const data = { "fecha": formatDate(this.fechaResumenDistrito.toString(), 'yyyy-MM-dd', 'en-ES') };
    this.service.filtroResumenPedidoDistrito(data).subscribe(res => {
      this.resumenDistrito = res["data"];
    });
  }

  resumenPedidoRepartidorFilter() {
    const data = { "fecha": formatDate(this.fechaResumenRepartidor.toString(), 'yyyy-MM-dd', 'en-ES') };
    this.service.filtroResumenPedidoRepartidor(data).subscribe(res => {
      this.resumenRepartidor = res["data"];
    });
  }

  getDistritos() {
    this.storage.get('DISTRITO_DATA').then(res => {
      if (!res) {
        this.storage.set('DISTRITO_DATA', []);
      }
    });
  }

  ngOnInit() {
    this.resumenPedidoCliente();
    this.resumenPedidoCategoria();
    this.resumenPedidoDistrito();
    this.resumenPedidoRepartidor();
    this.getDistritos();
  }
  ionViewWillEnter() {
    this.resumenPedidoCliente();
    this.resumenPedidoCategoria();
    this.resumenPedidoDistrito();
    this.resumenPedidoRepartidor();
  }

}
