import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PedidosResumenPage } from './pedidos-resumen.page';

const routes: Routes = [
  {
    path: '',
    component: PedidosResumenPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PedidosResumenPageRoutingModule {}
