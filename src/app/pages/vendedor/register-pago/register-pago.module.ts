import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RegisterPagoPageRoutingModule } from './register-pago-routing.module';

import { RegisterPagoPage } from './register-pago.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RegisterPagoPageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [RegisterPagoPage]
})
export class RegisterPagoPageModule { }
