import { Component, OnInit } from '@angular/core';
import { MenuController, NavController, ToastController } from '@ionic/angular';
import { AppComponent } from './../../../app.component';
import { ServiciosService } from './../../../services/servicios.service';
import { LoadingController } from "@ionic/angular";
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-stock',
  templateUrl: './stock.page.html',
  styleUrls: ['./stock.page.scss'],
})
export class StockPage implements OnInit {

  stock: any = [];
  stock_filter: any = [];
  loaderToShow: any;

  constructor(
    private menu: MenuController,
    private navCtrl: NavController,
    private appComponent: AppComponent,
    public loadingController: LoadingController,
    private service: ServiciosService,
    private storage: Storage,
    private toastCtrl: ToastController
  ) {
    this.menu.enable(true);
  }

  filterStock(ev: any) {
    this.stock = this.stock_filter;
    const val = ev.target.value;
    if (val && val.trim() != '') {
      this.stock = this.stock.filter((item) => {
        return (item["producto"]["nombre"].toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
  }

  goToPage(page) {
    this.navCtrl.navigateRoot(page);
  }

  refreshTotal() {
    var data = [];
    this.stock.forEach((element, index) => {
      var cantidad = 0;
      const input = document.getElementById(`cantidad${element['id']}`);
      (input["value"]) ? cantidad = parseInt(input["value"]) : cantidad = parseInt(element["cantidad"]);
      data[index] = {
        "id": element["id"],
        "cantidad": cantidad
      }
    });
    this.showLoader();
    this.service.updateStockAll(data).subscribe(res => {
      this.hideLoader();
      this.presentToast(res["message"]);
      this.listStock();
    });
  }

  listStock() {
    this.service.listStock().subscribe(res => {
      this.stock = res["data"];
      this.stock_filter = res["data"];
    });
  }

  refresh(id) {
    var cantidad = document.getElementById(`cantidad${id}`);
    const data = { "cantidad": cantidad["value"] }
    this.showLoader();
    this.service.updateStock(data, id).subscribe(res => {
      this.hideLoader();
      this.presentToast(res["message"]);
      this.listStock();
    });
  }

  showLoader() {
    this.loaderToShow = this.loadingController
      .create({
        message: "Cargando..."
      })
      .then(res => {
        res.present();
        res.onDidDismiss().then(dis => { });
      });
  }

  hideLoader() {
    this.loadingController.dismiss();
  }

  async presentToast(msg) {
    const toast = await this.toastCtrl.create({
      color: 'success',
      message: msg,
      duration: 2000
    });
    toast.present();
  }

  ngOnInit() {
    this.listStock();
  }

}
