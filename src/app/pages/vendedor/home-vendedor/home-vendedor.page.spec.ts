import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { HomeVendedorPage } from './home-vendedor.page';

describe('HomeVendedorPage', () => {
  let component: HomeVendedorPage;
  let fixture: ComponentFixture<HomeVendedorPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeVendedorPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(HomeVendedorPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
