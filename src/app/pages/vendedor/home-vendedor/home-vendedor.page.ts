import { Component, OnInit } from '@angular/core';
import { MenuController, NavController } from '@ionic/angular';
import { AppComponent } from './../../../app.component';
import { ServiciosService } from './../../../services/servicios.service';
import { LoadingController } from "@ionic/angular";
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home-vendedor',
  templateUrl: './home-vendedor.page.html',
  styleUrls: ['./home-vendedor.page.scss'],
})
export class HomeVendedorPage implements OnInit {

  name = "";

  constructor(
    private menu: MenuController,
    private navCtrl: NavController,
    private appComponent: AppComponent,
    public loadingController: LoadingController,
    private service: ServiciosService,
    private storage: Storage,
    private router: Router
  ) {
    this.menu.enable(true);
  }

  goToPage(page) {
    this.navCtrl.navigateRoot(page);
  }

  getName() {
    this.storage.get('USER_DATA').then(res => {
      if (res) {
        this.name = res["user"]["name"];
        const names = this.name.split(" ");
        this.name = names[0];
      }
    });
  }

  ngOnInit() {
    this.appComponent.setTipoUser();
    this.getName();
  }

}
