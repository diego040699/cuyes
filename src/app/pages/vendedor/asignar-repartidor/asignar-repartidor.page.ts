import { Component, OnInit } from '@angular/core';
import { AlertController, MenuController, NavController, ToastController } from '@ionic/angular';
import { AppComponent } from './../../../app.component';
import { ServiciosService } from './../../../services/servicios.service';
import { LoadingController } from "@ionic/angular";
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-asignar-repartidor',
  templateUrl: './asignar-repartidor.page.html',
  styleUrls: ['./asignar-repartidor.page.scss'],
})
export class AsignarRepartidorPage implements OnInit {

  distritos: any = [];
  repartidores: any = [];
  value_distrito = null;

  constructor(
    private menu: MenuController,
    private navCtrl: NavController,
    private appComponent: AppComponent,
    public loadingController: LoadingController,
    private service: ServiciosService,
    private storage: Storage,
    private toastCtrl: ToastController,
    private alertCtrl: AlertController
  ) {
    this.menu.enable(true);
  }

  back() {
    this.navCtrl.pop();
  }

  getDistritos() {
    this.storage.get('DISTRITO_DATA').then(res => {
      this.distritos = res;
    });
  }

  removeDistrito(index) {
    this.distritos.forEach((element, i) => {
      if (index == i) {
        this.distritos.splice(this.distritos.indexOf(element), 1);
      }
    });
    this.storage.set('DISTRITO_DATA', this.distritos);
    this.presentToast("Distrito eliminado", "success");
  }

  async presentToast(msg, color) {
    const toast = await this.toastCtrl.create({
      color: color,
      message: msg,
      duration: 2000
    });
    toast.present();
  }

  getRepartidores() {
    this.service.getRepartidores().subscribe(res => {
      this.repartidores = res["data"];
    });
  }

  save() {
    if (this.value_distrito) {
      const data = {
        "id_repartidor": this.value_distrito,
        "distritos": this.distritos
      };
      this.service.asignarRepartidor(data).subscribe(res => {
        this.storage.set('DISTRITO_DATA', []);
        this.showAlert(res["message"]);
      });
    } else {
      this.presentToast("Repartidor no seleccionado", "danger");
    }
  }

  async showAlert(message) {
    const alert = await this.alertCtrl.create({
      header: '',
      message: message,
      buttons: [
        {
          text: 'Aceptar',
          handler: () => {
            this.back();
          }
        }
      ]
    });
    await alert.present();
  }

  ngOnInit() {
    this.getDistritos();
    this.getRepartidores();
  }

}
