import { Component, OnInit } from '@angular/core';
import { MenuController, NavController, ToastController } from '@ionic/angular';
import { AppComponent } from '../../../app.component';
import { ServiciosService } from '../../../services/servicios.service';
import { LoadingController } from "@ionic/angular";
import { Storage } from '@ionic/storage';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { Clipboard } from '@ionic-native/clipboard/ngx';

declare var google;

@Component({
  selector: 'app-pedidos-detalle',
  templateUrl: './pedidos-detalle.page.html',
  styleUrls: ['./pedidos-detalle.page.scss'],
})
export class PedidosDetallePage implements OnInit {

  public fGroup: FormGroup;
  loaderToShow: any;
  map = null;
  marker = null;
  geoCoder = null;
  visibilityMap = false;
  id_pedido = 0;
  detalle = {
    "data_cliente": {},
    "data_pedido": {},
    "data_productos": [],
    "data_repartidor": {}
  };

  constructor(
    private menu: MenuController,
    private navCtrl: NavController,
    private appComponent: AppComponent,
    public loadingController: LoadingController,
    private service: ServiciosService,
    private storage: Storage,
    private fBuilder: FormBuilder,
    private geolocation: Geolocation,
    private route: ActivatedRoute,
    private toastCtrl: ToastController,
    private clipboard: Clipboard,
  ) {
    this.menu.enable(true);
  }

  copiarNumero() {
    this.clipboard.copy(this.detalle["data_pedido"]["numero_cuenta"]);
    this.presentToast("Numero de cuenta copiado", "success");
  }

  async presentToast(msg, color) {
    const toast = await this.toastCtrl.create({
      color: color,
      message: msg,
      duration: 2000
    });
    toast.present();
  }

  back() {
    // this.navCtrl.navigateRoot('pedidos-resumen');
    this.navCtrl.pop();
  }

  goToPage(page) {
    this.navCtrl.navigateForward(page);
  }

  detallePedido() {
    this.route.queryParamMap.subscribe(result => {
      this.id_pedido = result['params']["id"];
      const data = { "id_pedido": this.id_pedido };
      this.service.detallePedido(data).subscribe(res => {
        console.log(res["data"])
        this.detalle = res["data"];
      });
    });
  }

  /*Geolocalizacion*/

  async loadMap() {
    this.visibilityMap = true;
    var myLatLng = await this.getLocation();
    this.setMap(myLatLng);
  }

  setMap(myLatLng) {
    this.geoCoder = new google.maps.Geocoder();
    const mapEle: HTMLElement = document.getElementById('map');
    if (mapEle) {
      this.map = new google.maps.Map(mapEle, {
        center: myLatLng,
        zoom: 14
      });

      google.maps.event.addListenerOnce(this.map, 'idle', () => {
        this.addMarker(myLatLng.lat, myLatLng.lng);
      });
    }
  }

  private addMarker(lat: number, lng: number) {
    this.marker = new google.maps.Marker({
      draggable: false,
      position: { lat, lng },
      map: this.map,
      title: 'Hello mundo'
    });
  }

  private async getLocation() {
    const res = await this.geolocation.getCurrentPosition();
    return {
      lat: this.detalle["data_cliente"]["latitud"],
      lng: this.detalle["data_cliente"]["longitud"]
    };
  }

  /*Fin Geolocalizacion*/

  ngOnInit() {
    this.detallePedido();
  }

}
