import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PedidosDetallePage } from './pedidos-detalle.page';

describe('PedidosDetallePage', () => {
  let component: PedidosDetallePage;
  let fixture: ComponentFixture<PedidosDetallePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PedidosDetallePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PedidosDetallePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
