import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PedidosDetallePage } from './pedidos-detalle.page';

const routes: Routes = [
  {
    path: '',
    component: PedidosDetallePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PedidosDetallePageRoutingModule {}
