import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PedidosDetallePageRoutingModule } from './pedidos-detalle-routing.module';

import { PedidosDetallePage } from './pedidos-detalle.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PedidosDetallePageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [PedidosDetallePage]
})
export class PedidosDetallePageModule {}
