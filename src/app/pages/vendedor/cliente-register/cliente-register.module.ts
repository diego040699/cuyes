import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ClienteRegisterPageRoutingModule } from './cliente-register-routing.module';

import { ClienteRegisterPage } from './cliente-register.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ClienteRegisterPageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [ClienteRegisterPage]
})
export class ClienteRegisterPageModule { }
