import { Component, OnInit } from '@angular/core';
import { AlertController, MenuController, NavController, ToastController } from '@ionic/angular';
import { AppComponent } from './../../../app.component';
import { ServiciosService } from './../../../services/servicios.service';
import { LoadingController } from "@ionic/angular";
import { Storage } from '@ionic/storage';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

declare var google;

@Component({
  selector: 'app-cliente-register',
  templateUrl: './cliente-register.page.html',
  styleUrls: ['./cliente-register.page.scss'],
})
export class ClienteRegisterPage implements OnInit {

  public fGroup: FormGroup;
  loaderToShow: any;
  datos_reniec = "";
  distritos: any = [];
  grupo_cliente: any = [];

  map = null;
  marker = null;
  geoCoder = null;
  visibilityMap = false;

  constructor(
    private menu: MenuController,
    private navCtrl: NavController,
    private fBuilder: FormBuilder,
    private appComponent: AppComponent,
    public loadingController: LoadingController,
    private service: ServiciosService,
    private storage: Storage,
    private geolocation: Geolocation,
    private toastCtrl: ToastController,
    private alertCtrl: AlertController
  ) {
    this.menu.enable(true);
  }

  back() {
    this.navCtrl.pop();
  }

  getGrupoCliente() {
    this.service.getGrupoCliente().subscribe(res => {
      this.grupo_cliente = res["data"];
    });
  }

  getDistritos() {
    this.getGrupoCliente();
    this.service.getDistritos().subscribe(res => {
      this.distritos = res["data"];
    });
  }

  /*Geolocalizacion*/

  async loadMap() {
    this.visibilityMap = true;
    const myLatLng = await this.getLocation();
    this.setMap(myLatLng);
  }

  setMap(myLatLng) {
    this.geoCoder = new google.maps.Geocoder();
    const mapEle: HTMLElement = document.getElementById('map');
    if (mapEle) {
      this.map = new google.maps.Map(mapEle, {
        center: myLatLng,
        zoom: 14
      });

      google.maps.event.addListenerOnce(this.map, 'idle', () => {
        // this.hideLoader();
        this.geocodePosition(myLatLng);
        this.addMarker(myLatLng.lat, myLatLng.lng);
        google.maps.event.addListener(this.marker, 'dragend', () => {
          this.geocodePosition(this.marker.getPosition());
        });
      });
    }
  }

  geocodePosition(myLatLng) {
    this.geoCoder.geocode({
      latLng: myLatLng
    }, (responses) => {
      if (responses && responses.length > 0) {
        this.fGroup.get('direccion').setValue(responses[0].formatted_address);
        this.fGroup.get('lat').setValue(this.marker.position.lat());
        this.fGroup.get('lng').setValue(this.marker.position.lng());
      } else {

      }
    });
  }
  private addMarker(lat: number, lng: number) {
    this.marker = new google.maps.Marker({
      draggable: true,
      position: { lat, lng },
      map: this.map,
      title: 'Hello mundo'
    });
  }

  private async getLocation() {
    const res = await this.geolocation.getCurrentPosition();
    return {
      lat: res.coords.latitude,
      lng: res.coords.longitude
    };
  }

  /*Fin Geolocalizacion*/

  validarDni() {
    this.showLoader();
    const dni = (this.fGroup.value.dni).toString();
    if (dni.length == 8) {
      const data = { dni: dni };
      this.service.validarDni(data).subscribe(res => {
        this.hideLoader();
        if (res["data"]["message"]) {
          this.presentToast(res["data"]["message"], "danger");
          this.datos_reniec = "";
        } else {
          const nombres = `${res['data']['primerNombre']} ${res['data']['segundoNombre']} ${res['data']['apellidoPaterno']} ${res['data']['apellidoMaterno']}`
          this.datos_reniec = nombres;
        }
      });
    } else {
      this.presentToast("Dni debe contener 8 caracteres", "danger");
    }
  }

  async presentToast(msg, color) {
    const toast = await this.toastCtrl.create({
      color: color,
      message: msg,
      duration: 2000
    });
    toast.present();
  }

  save() {
    this.showLoader();
    var form = this.fGroup.value;
    const data = {
      data_user: {
        "id_tipousuario": 4,
        "dni": form.dni.toString(),
        "name": form.name,
        "celular": form.celular,
        "wsp": form.wsp
      },
      data_cliente: {
        "distrito": form.id_distrito,
        "direccion": form.direccion,
        "lat": form.lat,
        "lng": form.lng,
        "id_grupocliente": form.id_grupo_cliente
      }
    }

    this.service.registerCliente(data).subscribe(res => {
      this.hideLoader();
      if (res["data"]) {
        this.showAlert(res["message"]);
      } else {
        this.presentToast(res["message"], "danger")
      }
    });
  }

  async showAlert(message) {
    const alert = await this.alertCtrl.create({
      header: '',
      message: message,
      buttons: [
        {
          text: 'Aceptar',
          handler: () => {
            this.navCtrl.navigateRoot('cliente');
          }
        }
      ]
    });
    await alert.present();
  }

  showLoader() {
    this.loaderToShow = this.loadingController
      .create({
        message: "Cargando..."
      })
      .then(res => {
        res.present();
        res.onDidDismiss().then(dis => { });
      });
  }

  hideLoader() {
    this.loadingController.dismiss();
  }

  ngOnInit() {
    this.fGroup = this.fBuilder.group({
      dni: ["", [Validators.required]],
      name: ["", [Validators.required]],
      id_grupo_cliente: ["", [Validators.required]],
      id_distrito: ["", [Validators.required]],
      direccion: ["", [Validators.required]],
      lat: ["", [Validators.required]],
      lng: ["", [Validators.required]],
      celular: ["", [Validators.required, Validators.minLength(9)]],
      wsp: ["", [Validators.required, Validators.minLength(9)]]
    });
    this.getDistritos();
  }

}
