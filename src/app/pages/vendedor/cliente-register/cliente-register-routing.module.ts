import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ClienteRegisterPage } from './cliente-register.page';

const routes: Routes = [
  {
    path: '',
    component: ClienteRegisterPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ClienteRegisterPageRoutingModule {}
