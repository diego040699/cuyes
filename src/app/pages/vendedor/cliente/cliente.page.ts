import { Component, OnInit } from '@angular/core';
import { MenuController, NavController } from '@ionic/angular';
import { AppComponent } from './../../../app.component';
import { ServiciosService } from './../../../services/servicios.service';
import { LoadingController } from "@ionic/angular";
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-cliente',
  templateUrl: './cliente.page.html',
  styleUrls: ['./cliente.page.scss'],
})
export class ClientePage implements OnInit {

  clientes: any = [];
  clientes_filter: any = [];

  constructor(
    private menu: MenuController,
    private navCtrl: NavController,
    private appComponent: AppComponent,
    public loadingController: LoadingController,
    private service: ServiciosService,
    private storage: Storage
  ) {
    this.menu.enable(true);
  }

  goToPages(page) {
    this.navCtrl.navigateForward(page);
  }

  getClientes() {
    this.service.getClientes().subscribe(res => {
      this.clientes = res["data"];
      this.clientes_filter = res["data"];
    });
  }

  filterCliente(ev: any) {
    this.clientes = this.clientes_filter;
    const val = ev.target.value;
    if (val && val.trim() != '') {
      this.clientes = this.clientes.filter((item) => {
        return (item["cliente"].toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
  }

  ngOnInit() {
    this.getClientes();
  }

  ionViewWillEnter() {
    this.getClientes();
  }

}
