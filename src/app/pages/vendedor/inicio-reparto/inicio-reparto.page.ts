import { Component, OnInit } from '@angular/core';
import { AlertController, MenuController, NavController, ToastController } from '@ionic/angular';
import { AppComponent } from '../../../app.component';
import { ServiciosService } from '../../../services/servicios.service';
import { LoadingController } from "@ionic/angular";
import { Storage } from '@ionic/storage';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { formatDate } from '@angular/common';

@Component({
  selector: 'app-inicio-reparto',
  templateUrl: './inicio-reparto.page.html',
  styleUrls: ['./inicio-reparto.page.scss'],
})
export class InicioRepartoPage implements OnInit {

  public fGroup: FormGroup;
  loaderToShow: any;
  fecha_inicio: String = new Date().toISOString();
  hora_inicio: String = new Date().toISOString();
  descripcion = null;
  visibilityButton = true;
  inicio_reparto: any = [];
  resumen: any = [];

  constructor(
    private menu: MenuController,
    private navCtrl: NavController,
    private appComponent: AppComponent,
    public loadingController: LoadingController,
    private service: ServiciosService,
    private storage: Storage,
    private fBuilder: FormBuilder,
    private alertCtrl: AlertController,
    private toastCtrl: ToastController
  ) {
    this.menu.enable(true);
    console.log(this.fecha_inicio)
  }

  save() {
    const data = {
      "fecha_inicio": formatDate(this.fecha_inicio.toString(), 'yyyy-MM-dd', 'en-ES'),
      "hora_inicio": formatDate(this.fecha_inicio.toString(), 'H:mm:ss', 'en-ES'),
      "descripcion": this.descripcion
    };
    this.service.inicioReparto(data).subscribe(res => {
      if (res["data"]) {
        this.showAlert(res["message"]);
        this.getReparto();
      } else {
        this.presentToast(res["message"], 'danger');
      }
    });
  }

  resumenPedidoCliente() {
    this.service.resumenPedidoCliente().subscribe(res => {
      this.resumen = res["data"];
    });
  }

  habilitarInicioReparto() {
    var c = 0;
    this.resumen.forEach(element => {
      if (element["fecha"] == formatDate(this.fecha_inicio.toString(), 'dd/MM/yyyy', 'en-ES')) {
        c++;
      }
    });

    if (c > 0) {
      this.visibilityButton = true;
    } else {
      this.presentToast("No hay pedidos para el dia de hoy", "danger")
    }
  }

  getReparto() {
    this.service.getReparto().subscribe(res => {
      if (res["data"]) {
        this.inicio_reparto = res["data"];
        this.visibilityButton = false;
      }
    });
  }

  showAlert(message) {
    let alert = this.alertCtrl.create({
      message: message,
      header: "",
      buttons: ["OK"]
    });
    alert.then(alert => alert.present());
  }

  async presentToast(msg, color) {
    const toast = await this.toastCtrl.create({
      color: color,
      message: msg,
      duration: 2000
    });
    toast.present();
  }

  ngOnInit() {
    this.getReparto();
    this.resumenPedidoCliente();
  }


}
