import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { InicioRepartoPage } from './inicio-reparto.page';

const routes: Routes = [
  {
    path: '',
    component: InicioRepartoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class InicioRepartoPageRoutingModule {}
