import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { InicioRepartoPageRoutingModule } from './inicio-reparto-routing.module';

import { InicioRepartoPage } from './inicio-reparto.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    InicioRepartoPageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [InicioRepartoPage]
})
export class InicioRepartoPageModule { }
