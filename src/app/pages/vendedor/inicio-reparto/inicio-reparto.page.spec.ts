import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { InicioRepartoPage } from './inicio-reparto.page';

describe('InicioRepartoPage', () => {
  let component: InicioRepartoPage;
  let fixture: ComponentFixture<InicioRepartoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InicioRepartoPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(InicioRepartoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
