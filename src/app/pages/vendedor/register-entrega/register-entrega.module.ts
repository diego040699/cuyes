import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RegisterEntregaPageRoutingModule } from './register-entrega-routing.module';

import { RegisterEntregaPage } from './register-entrega.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RegisterEntregaPageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [RegisterEntregaPage]
})
export class RegisterEntregaPageModule { }
