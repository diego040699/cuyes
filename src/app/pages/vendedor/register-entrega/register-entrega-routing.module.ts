import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RegisterEntregaPage } from './register-entrega.page';

const routes: Routes = [
  {
    path: '',
    component: RegisterEntregaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RegisterEntregaPageRoutingModule {}
