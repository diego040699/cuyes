import { Component, OnInit } from '@angular/core';
import { AlertController, MenuController, NavController, ToastController } from '@ionic/angular';
import { AppComponent } from './../../../app.component';
import { ServiciosService } from './../../../services/servicios.service';
import { LoadingController } from "@ionic/angular";
import { Storage } from '@ionic/storage';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';

declare var google;

@Component({
  selector: 'app-cliente-detalle',
  templateUrl: './cliente-detalle.page.html',
  styleUrls: ['./cliente-detalle.page.scss'],
})
export class ClienteDetallePage implements OnInit {

  public fGroup: FormGroup;
  loaderToShow: any;
  distritos: any = [];
  grupo_cliente: any = [];
  datos_reniec = "";
  id_cliente = 0;

  map = null;
  marker = null;
  geoCoder = null;
  visibilityMap = false;
  deleted_at = null;

  constructor(
    private menu: MenuController,
    private navCtrl: NavController,
    private fBuilder: FormBuilder,
    private appComponent: AppComponent,
    public loadingController: LoadingController,
    private service: ServiciosService,
    private storage: Storage,
    private route: ActivatedRoute,
    private geolocation: Geolocation,
    private toastCtrl: ToastController,
    private alertCtrl: AlertController
  ) {
    this.menu.enable(true);
  }

  deshabilitar() {
    this.showLoader();
    this.service.userDestroy(this.id_cliente).subscribe(res => {
      this.hideLoader();
      this.showAlert(res["message"]);
    });
  }

  habilitar() {
    this.showLoader();
    this.service.habilitarUser(this.id_cliente).subscribe(res => {
      this.hideLoader();
      this.showAlert(res["message"]);
    })
  }

  getGrupoCliente() {
    this.service.getGrupoCliente().subscribe(res => {
      this.grupo_cliente = res["data"];
    });
  }

  getDistritos() {
    this.getGrupoCliente();
    this.service.getDistritos().subscribe(res => {
      this.distritos = res["data"];
      this.getUser();
    });
  }

  back() {
    this.navCtrl.pop();
  }

  save() {
    this.showLoader();
    var form = this.fGroup.value;
    const data = {
      data_user: {
        "dni": form.dni.toString(),
        "name": form.name,
        "celular": form.celular,
        "wsp": form.wsp
      },
      data_cliente: {
        "distrito": form.id_distrito,
        "direccion": form.direccion,
        "lat": form.lat,
        "lng": form.lng,
        "id_grupocliente": form.id_grupo_cliente
      }
    }

    this.service.updateCliente(data, this.id_cliente).subscribe(res => {
      this.hideLoader();
      if (res["data"]) {
        this.showAlert(res["message"]);
      } else {
        this.presentToast(res["message"], "danger")
      }
    });
  }

  async showAlert(message) {
    const alert = await this.alertCtrl.create({
      header: '',
      message: message,
      buttons: [
        {
          text: 'Aceptar',
          handler: () => {
            this.navCtrl.navigateRoot('cliente');
          }
        }
      ]
    });
    await alert.present();
  }

  getUser() {
    this.route.queryParamMap.subscribe(result => {
      this.id_cliente = result['params']["id"];
      const data = { id_cliente: this.id_cliente };
      this.service.detalleCliente(data).subscribe(res => {
        this.deleted_at = res["data"]["deleted_at"];
        this.fGroup.get('id_grupo_cliente').setValue(res["data"]["id_grupocliente"]);
        this.fGroup.get('dni').setValue(res["data"]["dni"]);
        this.fGroup.get('name').setValue(res["data"]["nombres"]);
        this.fGroup.get('id_distrito').setValue(res["data"]["id_distrito"]);
        this.fGroup.get('direccion').setValue(res["data"]["direccion"]);
        this.fGroup.get('celular').setValue(res["data"]["celular"]);
        this.fGroup.get('wsp').setValue(res["data"]["whatsapp"]);
        this.fGroup.get('lat').setValue(parseFloat(res["data"]["lat"]));
        this.fGroup.get('lng').setValue(parseFloat(res["data"]["long"]));
      });
    });
  }

  /*Geolocalizacion*/

  async loadMap() {
    this.visibilityMap = true;
    var myLatLng;
    if (this.fGroup.value.lat && this.fGroup.value.lng) {
      myLatLng = {
        lat: this.fGroup.value.lat,
        lng: this.fGroup.value.lng
      };
      window.setTimeout(() => {
        this.setMap(myLatLng);
      }, 100);
    } else {
      myLatLng = await this.getLocation();
      window.setTimeout(() => {
        this.setMap(myLatLng);
      }, 100);
    }
  }

  setMap(myLatLng) {
    this.geoCoder = new google.maps.Geocoder();
    const mapEle: HTMLElement = document.getElementById('map');
    if (mapEle) {
      this.map = new google.maps.Map(mapEle, {
        center: myLatLng,
        zoom: 14
      });

      google.maps.event.addListenerOnce(this.map, 'idle', () => {
        // this.hideLoader();
        this.geocodePosition(myLatLng);
        this.addMarker(myLatLng.lat, myLatLng.lng);
        google.maps.event.addListener(this.marker, 'dragend', () => {
          this.geocodePosition(this.marker.getPosition());
        });
      });
    }
  }

  geocodePosition(myLatLng) {
    this.geoCoder.geocode({
      latLng: myLatLng
    }, (responses) => {
      if (responses && responses.length > 0) {
        this.fGroup.get('direccion').setValue(responses[0].formatted_address);
        this.fGroup.get('lat').setValue(this.marker.position.lat());
        this.fGroup.get('lng').setValue(this.marker.position.lng());
      } else {

      }
    });
  }
  private addMarker(lat: number, lng: number) {
    this.marker = new google.maps.Marker({
      draggable: true,
      position: { lat, lng },
      map: this.map,
      title: 'Hello mundo'
    });
  }

  private async getLocation() {
    const res = await this.geolocation.getCurrentPosition();
    return {
      lat: res.coords.latitude,
      lng: res.coords.longitude
    };
  }

  /*Fin Geolocalizacion*/

  validarDni() {
    this.showLoader();
    const dni = (this.fGroup.value.dni).toString();
    if (dni.length == 8) {
      const data = { dni: dni };
      this.service.validarDni(data).subscribe(res => {
        this.hideLoader();
        if (res["data"]["message"]) {
          this.presentToast(res["data"]["message"], "danger");
          this.datos_reniec = "";
        } else {
          const nombres = `${res['data']['primerNombre']} ${res['data']['segundoNombre']} ${res['data']['apellidoPaterno']} ${res['data']['apellidoMaterno']}`
          this.datos_reniec = nombres;
        }
      });
    } else {
      this.presentToast("Dni debe contener 8 caracteres", "danger");
    }
  }

  async presentToast(msg, color) {
    const toast = await this.toastCtrl.create({
      color: color,
      message: msg,
      duration: 2000
    });
    toast.present();
  }

  showLoader() {
    this.loaderToShow = this.loadingController
      .create({
        message: "Cargando..."
      })
      .then(res => {
        res.present();
        res.onDidDismiss().then(dis => { });
      });
  }

  hideLoader() {
    this.loadingController.dismiss();
  }

  ngOnInit() {
    this.fGroup = this.fBuilder.group({
      dni: ["", [Validators.required]],
      name: ["", [Validators.required]],
      id_grupo_cliente: ["", [Validators.required]],
      id_distrito: ["", [Validators.required]],
      direccion: ["", [Validators.required]],
      lat: ["", [Validators.required]],
      lng: ["", [Validators.required]],
      celular: ["", [Validators.required, Validators.minLength(9)]],
      wsp: ["", [Validators.required, Validators.minLength(9)]]
    });
    this.getDistritos();
  }

}
