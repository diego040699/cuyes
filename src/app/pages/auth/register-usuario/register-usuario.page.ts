import { Component, OnInit } from '@angular/core';
import { MenuController, NavController } from '@ionic/angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ServiciosService } from './../../../services/servicios.service';
import { LoadingController, AlertController } from "@ionic/angular";
import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
  selector: 'app-register-usuario',
  templateUrl: './register-usuario.page.html',
  styleUrls: ['./register-usuario.page.scss'],
})
export class RegisterUsuarioPage implements OnInit {

  public fGroup: FormGroup;
  loaderToShow: any;
  pass: string = '';
  password_type: string = 'password';
  iconpassword = 'eye-off';

  constructor(
    private menu: MenuController,
    private navCtrl: NavController,
    public loadingController: LoadingController,
    private fBuilder: FormBuilder,
    private alertCtrl: AlertController,
    private service: ServiciosService,
    private authService: AuthenticationService
  ) {
    this.menu.enable(false);
  }

  showLoader() {
    this.loaderToShow = this.loadingController
      .create({
        message: "Cargando..."
      })
      .then(res => {
        res.present();
        res.onDidDismiss().then(dis => { });
      });
  }

  hideLoader() {
    this.loadingController.dismiss();
  }

  showAlert(message) {
    let alert = this.alertCtrl.create({
      message: message,
      header: "",
      buttons: ["OK"]
    });
    alert.then(alert => alert.present());
  }

  back() {
    this.navCtrl.pop();
    // this.navCtrl.navigateRoot('login');
  }

  registerUser() {
    if (this.fGroup.value.dni != "00000000") {
      if (this.fGroup.value.password == this.fGroup.value.confirmPassword) {
        this.showLoader();
        this.service.registerUser(this.fGroup.value).subscribe(res => {
          if (res["data"]) {
            this.hideLoader();
            // this.login(res["message"]);
            this.authService.validarLogin(this.fGroup.value);
          } else {
            this.hideLoader();
            this.showAlert(res["message"]);
          }
        });
      } else {
        this.showAlert("Contraseña no coincide");
      }
    } else {
      this.showAlert("DNI incorrecto");
    }
  }

  async login(message) {
    const alert = await this.alertCtrl.create({
      header: '',
      message: message,
      buttons: [
        {
          text: 'Ingresar',
          cssClass: 'secondary',
          handler: () => {
            this.authService.validarLogin(this.fGroup.value);
          }
        }
      ]
    });
    await alert.present();
  }

  addEyes() {
    this.pass = this.fGroup.get('password').value;
  }

  togglePasswordMode() {
    this.password_type = this.password_type === 'text' ? 'password' : 'text';
    this.iconpassword = this.iconpassword === 'eye-off' ? 'eye' : 'eye-off';
  }

  ngOnInit() {
    this.fGroup = this.fBuilder.group({
      dni: ["", [Validators.required, Validators.maxLength(8), Validators.minLength(8)]],
      name: ["", [Validators.required]],
      password: ["", [Validators.required, Validators.minLength(6)]],
      confirmPassword: ["", [Validators.required, Validators.minLength(6)]],
    });
  }

}
