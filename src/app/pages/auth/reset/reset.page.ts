import { Component, OnInit } from '@angular/core';
import { MenuController, NavController } from '@ionic/angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ServiciosService } from './../../../services/servicios.service';
import { LoadingController, AlertController } from "@ionic/angular";
import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
  selector: 'app-reset',
  templateUrl: './reset.page.html',
  styleUrls: ['./reset.page.scss'],
})
export class ResetPage implements OnInit {

  public fGroup: FormGroup;
  loaderToShow: any;

  constructor(
    private menu: MenuController,
    private navCtrl: NavController,
    public loadingController: LoadingController,
    private fBuilder: FormBuilder,
    private alertCtrl: AlertController,
    private service: ServiciosService,
    private authService: AuthenticationService
  ) {
    this.menu.enable(false);
  }

  ngOnInit() {
    this.fGroup = this.fBuilder.group({
      dni: ["", [Validators.required, Validators.maxLength(8), Validators.minLength(8)]],
      correo: ["", [Validators.required, Validators.email]]
    });
  }

  save(){
    
  }

  back() {
    this.navCtrl.pop();
  }

}
