import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MenuController, NavController } from '@ionic/angular';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { Router } from '@angular/router';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook/ngx';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  public fGroup: FormGroup;
  pass: string = '';
  constructor(
    private fBuilder: FormBuilder,
    private menu: MenuController,
    private authService: AuthenticationService,
    private navCtrl: NavController,
    private router: Router,
    private fb: Facebook
  ) {
    this.menu.enable(false);
  }

  password_type: string = 'password';
  iconpassword = 'eye-off';

  togglePasswordMode() {
    this.password_type = this.password_type === 'text' ? 'password' : 'text';
    this.iconpassword = this.iconpassword === 'eye-off' ? 'eye' : 'eye-off';
  }

  addEyes() {
    this.pass = this.fGroup.get('password').value;
  }


  validarLogin() {
    this.authService.validarLogin(this.fGroup.value);
  }

  registerUser() {
    this.router.navigate(['register-usuario']);
  }

  reset() {
    this.router.navigate(['reset']);
  }

  facebook() {
    this.fb.login(['public_profile', 'user_friends', 'email'])
      .then((res: FacebookLoginResponse) => {
        if (res.status == "connected") {
          alert(res);
          console.log('Logged into Facebook!', res)
        }else{
          alert("Ocurrio un error");
        }
      })
      .catch(e => console.log('Error logging into Facebook', e));


    this.fb.logEvent(this.fb.EVENTS.EVENT_NAME_ADDED_TO_CART);
  }

  ngOnInit() {
    this.fGroup = this.fBuilder.group({
      dni: ['', [Validators.required, Validators.minLength(8), Validators.maxLength(8)]],
      password: ['', [Validators.required]]
    });
  }

}
