import { Component } from '@angular/core';
import { Router, RouterEvent, ActivatedRoute, } from '@angular/router';
import { Platform, NavController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AuthenticationService } from './services/authentication.service';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {

  activePage: any;
  tipo_user = 0;

  public appPages = [
    {
      title: 'Inicio',
      url: '/home',
      page: 'home',
      icon: 'home',
      type: 4
    },
    {
      title: 'CUYHistorial',
      url: '/historial',
      page: 'historial',
      icon: 'timer',
      type: 4
    },
    {
      title: 'CUYPerfil',
      url: '/perfil-cliente',
      page: 'perfil-cliente',
      icon: 'person',
      type: 4
    },
    {
      title: 'Datos Comerciales',
      url: '/datos-comerciales',
      page: 'datos-comerciales',
      icon: 'paper-plane',
      type: 4
    },
    {
      title: 'Inicio',
      url: '/home-vendedor',
      page: 'home-vendedor',
      icon: 'home',
      type: 5
    },
    {
      title: 'Stock',
      url: '/stock',
      page: 'stock',
      icon: 'refresh-circle',
      type: 5
    },
    {
      title: 'CUYResumen',
      url: '/pedidos-resumen',
      page: 'pedidos-resumen',
      icon: 'document',
      type: 5
    },
    {
      title: 'CUYReparto',
      url: '/inicio-reparto',
      page: 'inicio-reparto',
      icon: 'document',
      type: 5
    },
    {
      title: 'CUYClientes',
      url: '/cliente',
      page: 'cliente',
      icon: 'people',
      type: 5
    },
    {
      title: 'Inicio',
      url: '/home-repartidor',
      page: 'home-repartidor',
      icon: 'home',
      type: 2
    }
  ];

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private navCtrl: NavController,
    private authService: AuthenticationService,
    private router: Router,
    private storage: Storage
  ) {
    this.initializeApp();
    this.activePage = this.appPages[0];
  }

  setTipoUser() {
    this.storage.get('USER_DATA').then(res => {
      if (res) {
        this.tipo_user = res["user"]["id_tipousuario"];
      }
    });
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();

      this.storage.get('USER_DATA').then(res => {
        if (res) {
          this.authService.authenticationState.subscribe(state => {
            if (state) {
              // this.tipo_user = res["user"]["id_tipousuario"];
              // console.log(this.tipo_user);
              // if (this.tipo_user == 4) {
              //   this.navCtrl.navigateRoot("home");
              // } else if (this.tipo_user == 5) {
              //   this.navCtrl.navigateRoot("home-vendedor");
              // }
            } else {
              this.router.navigate(['login']);
            }
          });
        }
      });
    });
  }

  openPage(page) {
    this.navCtrl.navigateRoot(page.url);
    this.activePage = page;
  }

  openPageRoot(page) {
    this.navCtrl.navigateRoot(page.url);
    this.activePage = page;
  }

  checkActivatePage(page) {
    return page == this.activePage;
  }

  logout() {
    this.authService.logout();
  }

  login() {
    this.navCtrl.navigateRoot('login');
  }
}
